import json

from got.browser import TweetOpener


def convOpen(kw):
    print("===========")
    print("Keyword :" + kw)
    ids = []
    inputfilename = "DATA/New/" + kw + "ConvIds.txt"
    # inputfilename = 'immigratiConvIds.txt'
    with open(inputfilename) as f:
        data = json.load(f)
        tweetOpener = TweetOpener()
        tweetOpener.openFile(kw)

        for id in data:
            if id not in ids:
                ids.append(id)

        print("Total ids : %d" % len(data))
        print("Clean ids : %d" % len(ids))

        # return len(ids)
        tweetOpener.open_tweets(ids)
        try:
            tweetOpener.close()
        except Exception, e:
            print e



def main():
    keywords = ["amnio", "Amniocentesis", "blood test", "counsyl", "diagnostic test", "FISH test",
                "Harmony test", "karyotype", "MaterniT21", "microarray test", "Natera",
                "NIPT", "Non invasive prenatal testing", "Non-invasive prenatal testing",
                "Nuchal translucency", "Panorama test", "Prenatal diagnosis", "quad screen", "Sequenom"]
    # keywords = ["amnio", "Amniocentesis", "blood test", "counsyl", "diagnostic test", "FISH test"]

    keywords = ["Natera"]

    # keywords = ["a"]

    for kw in keywords:
        convOpen(kw)


if __name__ == '__main__':
    main()
