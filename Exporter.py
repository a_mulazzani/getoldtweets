# -*- coding: utf-8 -*-
import sys, getopt, datetime, codecs

if sys.version_info[0] < 3:
    import got
else:
    import got3 as got


def main(argv):
    # if len(argv) == 0:
    # 	print('You must pass some parameters. Use \"-h\" to help.')
    # 	return

    if len(argv) == 1 and argv[0] == '-h':
        f = open('exporter_help_text.txt', 'r')
        print f.read()
        f.close()

        return

    try:

        keywords = ["amnio","Amniocentesis","blood test","counsyl","diagnostic test","FISH test",
                    "Harmony test","karyotype","MaterniT21","microarray test","Natera",
                    "NIPT","Non invasive prenatal testing","Non-invasive prenatal testing",
                    "Nuchal translucency","Panorama test","Prenatal diagnosis","quad screen","Sequenom"]
        keywords = ["microarray test","Natera",
                    "NIPT","Non invasive prenatal testing","Non-invasive prenatal testing",
                    "Nuchal translucency","Panorama test","Prenatal diagnosis","quad screen","Sequenom", "counsyl"]

        # keywords = ["Non-invasive prenatal testing",
        #             "Nuchal translucency","Panorama test","Prenatal diagnosis","quad screen","Sequenom"]

        keywords = ["cousyl"]
        opts, args = getopt.getopt(argv, "", (
            "username=", "near=", "within=", "since=", "until=", "querysearch=", "toptweets", "maxtweets=", "output="))

        for kw in keywords:
            print("=============================")
            print("Keyword = %s " % kw)
            # tweetCriteria = got.manager.TweetCriteria().setQuerySearch("amnio").setSince("2011-01-01").setUntil(
            #      "2018-03-31")

            tweetCriteria = got.manager.TweetCriteria().setQuerySearch("counsyl").setSince("2011-01-01").setUntil(
                 "2018-03-31")
            # tweetCriteria = got.manager.TweetCriteria().setQuerySearch(kw).setMaxTweets(10)
            outputFileName = "output_got.csv"

            for opt, arg in opts:
                if opt == '--username':
                    tweetCriteria.username = arg

                elif opt == '--since':
                    tweetCriteria.since = arg

                elif opt == '--until':
                    tweetCriteria.until = arg

                elif opt == '--querysearch':
                    tweetCriteria.querySearch = arg

                elif opt == '--toptweets':
                    tweetCriteria.topTweets = True

                elif opt == '--maxtweets':
                    tweetCriteria.maxTweets = int(arg)

                elif opt == '--near':
                    tweetCriteria.near = '"' + arg + '"'

                elif opt == '--within':
                    tweetCriteria.within = '"' + arg + '"'

                elif opt == '--within':
                    tweetCriteria.within = '"' + arg + '"'

                elif opt == '--output':
                    outputFileName = arg

            outputFileName = 'DATA/New/' + tweetCriteria.querySearch + '.csv'
            outputFile = codecs.open(outputFileName, "w+", "utf-8")

            outputFile.write('username;date;text;hashtags;id;')

            print('Searching...\n')

            def receiveBuffer(tweets):
                for t in tweets:
                    outputFile.write(('\n%s;%s;"%s";%s;"%s";' % (
                        t.username, t.date.strftime("%Y-%m-%d %H:%M"), t.text, t.hashtags, t.id)))
                outputFile.flush()
                print('More %d saved on file...\n' % len(tweets))

            got.manager.TweetManager.getTweets(tweetCriteria, receiveBuffer)


    finally:
        outputFile.close()
        print('Done. Output file generated "%s".' % outputFileName)


if __name__ == '__main__':
    main(sys.argv[1:])
