import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import pymysql.cursors

# plt.rcParams["figure.figsize"] = 15, 6
plt.rcParams["figure.figsize"] = 10, 6

totals = []
years = []

keywords = ["amnio", "Amniocentesis", "blood test", "counsyl", "diagnostic test", "FISH test",
            "Harmony test", "karyotype", "MaterniT21", "microarray test", "Natera",
            "NIPT", "Non invasive prenatal testing", "Non-invasive prenatal testing",
            "Nuchal translucency", "Panorama test", "Prenatal diagnosis", "quad screen", "Sequenom"]

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='',
                             db='thesis2',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


def plotDemo():
    dates = []
    for year in range(2011, 2018):
        for month in range(1, 12):
            dates.append(dt.datetime(year=year, month=month, day=1))

    y = [10, 180, 153, 80, 11, 92, 201, 74, 24]
    # plt.plot(dates[:9], y)
    plt.axhline(y=0, color='r', linestyle='dotted')

    plt.show()


def getDataFromSingle(kw):
    totals = []
    with connection.cursor() as cursor:
        # sql = "SELECT count(*) as Totale, year(date) as Anno FROM `data_conv2` GROUP by year(date)"
        # sql = "SELECT * FROM `data` WHERE `email`=%s"
        sql = "SELECT count(*) as Totale , year(date) as anno, month(date) as mese FROM `" + kw + "` WHERE id != 'id' GROUP by year(date), month(date)"
        cursor.execute(sql)
        result = cursor.fetchall()
        for data in result:
            totals.append(data["Totale"])

    return totals


def getYMSingle(kw):
    years = []
    with connection.cursor() as cursor:
        sql = "SELECT DISTINCT month(date) as mese from `" + kw + "` where year(date) = %s AND id != 'id' order by mese asc"
        for year in range(2011, 2019):
            months = []
            cursor.execute(sql, year)
            result = cursor.fetchall()
            for data in result:
                months.append(data["mese"])
            years.append((year, months))
    return years


def getData():
    totals = []
    with connection.cursor() as cursor:
        # sql = "SELECT count(*) as Totale, year(date) as Anno FROM `data_conv2` GROUP by year(date)"
        # sql = "SELECT * FROM `data` WHERE `email`=%s"
        sql = "SELECT count(*) as Totale, year(date) as anno, month(date) as mese FROM `data_conv2` GROUP by year(date), month(date)"
        cursor.execute(sql)
        result = cursor.fetchall()
        for data in result:
            totals.append(data["Totale"])

    return totals


def getAvgSentiment():
    totals = []
    with connection.cursor() as cursor:
        # sql = "SELECT count(*) as Totale, year(date) as Anno FROM `data_conv2` GROUP by year(date)"
        # sql = "SELECT * FROM `data` WHERE `email`=%s"
        sql = "SELECT AVG(polar) as media, year(date) as anno, month(date) as mese FROM `data_conv2` GROUP by year(date), month(date)"
        cursor.execute(sql)
        result = cursor.fetchall()
        for data in result:
            totals.append(data["media"])

    return totals


def getAvgSentimentSingle(kw):
    totals = []
    with connection.cursor() as cursor:
        # sql = "SELECT count(*) as Totale, year(date) as Anno FROM `data_conv2` GROUP by year(date)"
        # sql = "SELECT * FROM `data` WHERE `email`=%s"
        sql = "SELECT AVG(polar) as media, year(date) as anno, month(date) as mese FROM `" + kw + "` WHERE id != 'id' GROUP by year(date), month(date)"
        cursor.execute(sql)
        result = cursor.fetchall()
        for data in result:
            totals.append(data["media"])

    return totals


def getYM():
    with connection.cursor() as cursor:
        sql = "SELECT DISTINCT month(date) as mese from data_conv2 where year(date) = %s order by mese asc"
        for year in range(2011, 2019):
            months = []
            cursor.execute(sql, year)
            result = cursor.fetchall()
            for data in result:
                months.append(data["mese"])
            years.append((year, months))

        # for year, months in years:
        #     print "Year %d" % year
        #     print "Months len %d " % len(months)


def plot():
    dates = []

    totals = getData()
    getYM()
    # for year in range(2011, 2019):
    #     dates.append(dt.datetime(year=year, month=1, day=1))

    for year, months in years:
        # dates.append(year)
        for m in months:
            dates.append(dt.datetime(year=year, month=m, day=1))

    print len(dates)
    print(totals)

    plt.plot(dates, totals)
    plt.title("Conversation total data ")
    plt.show()


def plotSingle():
    for kw in keywords:
        dates = []
        table2 = kw + "_conv"
        totals = getDataFromSingle(table2)
        years = getYMSingle(table2)

        for year, months in years:
            # dates.append(year)
            for m in months:
                dates.append(dt.datetime(year=year, month=m, day=1))

        print len(totals)
        print len(dates)

        plt.plot(dates, totals)
        plt.title("Conversation Data for keyword %s" % kw)
        # file = open('DATA/Figures/%s.png' % kw, 'w+')
        plt.savefig("Figures/conv/%s.png" % kw)
        plt.show()


def plotSentiment():
    dates = []

    totals = getAvgSentiment()
    getYM()
    # for year in range(2011, 2019):
    #     dates.append(dt.datetime(year=year, month=1, day=1))

    for year, months in years:
        # dates.append(year)
        for m in months:
            dates.append(dt.datetime(year=year, month=m, day=1))

    print len(dates)
    print(totals)

    # plt.axhline(y=0, color='r', linestyle='dotted',)
    plt.hlines(y=0, linestyles='dotted', xmin=dt.datetime(year=2011, month=3, day=1),
               xmax=dt.datetime(year=2018, month=5, day=1))
    plt.plot(dates, totals)
    plt.title("Conversation total data ")
    # plt.savefig("Figures/conv/convSent.png")
    # plt.savefig("Figures/Sent.png")

    plt.show()


def plotEverySentiment():
    for kw in keywords:
        dates = []
        plt.title("Sentiment data for keyword %s" % kw)

        table2 = kw + "_conv"
        totals = getAvgSentimentSingle(kw)
        years = getYMSingle(kw)

        for year, months in years:
            # dates.append(year)
            for m in months:
                dates.append(dt.datetime(year=year, month=m, day=1))

        plt.plot(dates, totals, "o-")
        plt.title("Sentiment data for keyword %s" % kw)
        plt.savefig("Figures/Sentiment/%s.png" % kw)
        plt.show()
