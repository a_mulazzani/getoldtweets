import pymysql.cursors

keywords = ["amnio", "Amniocentesis", "blood test", "counsyl", "diagnostic test", "FISH test",
            "Harmony test", "karyotype", "MaterniT21", "microarray test", "Natera",
            "NIPT", "Non invasive prenatal testing", "Non-invasive prenatal testing",
            "Nuchal translucency", "Panorama test", "Prenatal diagnosis", "quad screen", "Sequenom"]


def checkWords():
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='',
                                 db='thesis2',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    # sql2 = "UPDATE `data_conv2` SET `data_id`=`pertinent`= 1 WHERE `data_id` = %d
    sql2 = "UPDATE `data_conv2` SET `pertinent`= 1 WHERE `data_id` = %s"
    connection.autocommit(0)

    with connection.cursor() as cursor:
        sql = "SELECT data_id, text FROM `data_conv2`"
        # sql = "SELECT * FROM `data` WHERE `email`=%s"
        cursor.execute(sql)
        result = cursor.fetchall()
        totResult = len(result)
        i = 0
        for text in result:
            i = i + 1
            if i % 500 == 0:
                print("%d/%d" % (i, totResult))

            string = text["text"]
            for kw in keywords:
                if kw in string:
                    print "True"
                    cursor.execute(sql2, text["data_id"])

        connection.commit()
