from dataset.languageChecker import checkLanguage
from dataset.plotter import plot, plotSingle, plotSentiment, plotDemo, plotEverySentiment
from dataset.sentimentAnalys import demo, analyze, startComplete
from dataset.wordChecker import checkWords


def main():
    # checkLanguage()
    # checkWords()
    # plotDemo()
    # plotSingle()
    demo()
    # analyze()
    # plotSentiment()
    #plotEverySentiment()
    # startComplete()


if __name__ == '__main__':
    main()
