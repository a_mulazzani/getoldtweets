from decimal import Decimal

import pymysql
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer



keywords = ["amnio", "Amniocentesis", "blood test", "counsyl", "diagnostic test", "FISH test",
            "Harmony test", "karyotype", "MaterniT21", "microarray test", "Natera",
            "NIPT", "Non invasive prenatal testing", "Non-invasive prenatal testing",
            "Nuchal translucency", "Panorama test", "Prenatal diagnosis", "quad screen", "Sequenom"]

def analyze():
    analyzer = SentimentIntensityAnalyzer()

    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='',
                                 db='thesis2',
                                 charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)
    sql2 = "UPDATE `data_conv2` SET `polar`= %s WHERE `data_id` = %s"
    connection.autocommit(0)

    with connection.cursor() as cursor:
        sql = "SELECT data_id, text FROM `data_conv2`"
        # sql = "SELECT * FROM `data` WHERE `email`=%s"
        cursor.execute(sql)
        result = cursor.fetchall()
        totResult = len(result)
        i = 0
        for text in result:
            i = i + 1
            if i % 500 == 0:
                print("%d/%d" % (i, totResult))

            string = text["text"]
            # print "True"
            try:
                vs = analyzer.polarity_scores(string)
                # value = Decimal(vs["compound"])
                datas = (vs["compound"], text["data_id"])
                # print(datas)
                cursor.execute(sql2, datas)
                # print("Ok")
            except Exception, e:
                # pass
                print(e)
                # print(string)
        connection.commit()


def analyzeSingleFile(kw, analyzer, connection):





    sql2 = "UPDATE `"+kw+"` SET `polar`= %s WHERE `data_id` = %s"
    connection.autocommit(0)

    with connection.cursor() as cursor:

        sqlFix = "ALTER TABLE `"+kw+"` ADD `polar` DECIMAL(6,5) NULL DEFAULT '0' AFTER `id`;"
        cursor.execute(sqlFix)
        sql = "SELECT data_id, text FROM `"+ kw+ "`"
        # sql = "SELECT * FROM `data` WHERE `email`=%s"
        cursor.execute(sql)
        result = cursor.fetchall()
        totResult = len(result)
        i = 0
        for text in result:
            i = i + 1
            if i % 500 == 0:
                print("%d/%d" % (i, totResult))

            string = text["text"]
            # print "True"
            try:
                vs = analyzer.polarity_scores(string)
                # value = Decimal(vs["compound"])
                datas = (vs["compound"], text["data_id"])
                # print(datas)
                cursor.execute(sql2, datas)
                # print("Ok")
            except Exception, e:
                # pass
                print(e)
                # print(string)
        connection.commit()


def demo():
    analyzer = SentimentIntensityAnalyzer()
    sentence = "The weather is great today"
    sentence2 = "I totally hate you."
    sentence3 = "My Name is John."
    vs = analyzer.polarity_scores(sentence)
    vs2 = analyzer.polarity_scores(sentence2)
    scores3 = analyzer.polarity_scores(sentence3)
    print(vs)
    print(vs2)
    print(scores3)


def startComplete():
    analyzer = SentimentIntensityAnalyzer()

    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='',
                                 db='thesis2',
                                 charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor)

    for kw in keywords:
        kw = kw+"_conv"
        analyzeSingleFile(kw, analyzer, connection)

    connection.close()
