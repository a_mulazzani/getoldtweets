from langdetect import detect
import pymysql.cursors


def checkLanguage():
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='',
                                 db='thesis2',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    sql2 = "INSERT INTO `data_conv2`(`username`, `date`, `text`, `id`,`convID`) VALUES (%s,%s,%s,%s,%s)"
    connection.autocommit(0)

    with connection.cursor() as cursor:
        # cursor.execute(sql, ("a", "a", "a", "a"))
        # connection.commit()
        # Read a single record
        sql = "SELECT * FROM `data_conv`"
        # sql = "SELECT * FROM `data` WHERE `email`=%s"
        cursor.execute(sql)
        result = cursor.fetchall()
        totResult = len(result)
        # print "0/%d" % totResult
        i = 0
        for res in result:
            try:
                lang = detect(res["text"])
                i = i + 1
                if i % 500 == 0:
                    print "%d/%d" % (i, totResult)
                if lang == 'en':
                    user = res["username"]
                    date = res["date"]
                    text = res["text"]
                    id = res["id"].replace("\"", "")
                    convID = res["convID"].replace("\"", "")

                    datas = (user, date, text, id,convID)
                    # with connection.cursor() as cursor:
                    cursor.execute(sql2, datas)

            except Exception, e:
                print e

    print "i'm done"
    connection.commit()
    connection.close()

    # lang = detect("This is english, obviously")
    # print(lang)



