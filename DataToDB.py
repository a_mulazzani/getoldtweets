import csv
import pymysql.cursors
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from langdetect import detect

keywords = ["amnio", "Amniocentesis", "blood test", "counsyl", "diagnostic test", "FISH test",
            "Harmony test", "karyotype", "MaterniT21", "microarray test", "Natera",
            "NIPT", "Non invasive prenatal testing", "Non-invasive prenatal testing",
            "Nuchal translucency", "Panorama test", "Prenatal diagnosis", "quad screen", "Sequenom"]


# keywords = ["amnio"]


def main():
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='',
                                 db='thesis2',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    sql2 = "CREATE TABLE IF NOT EXISTS `%s` (`data_id` int(11) NOT NULL," \
           "  `username` varchar(25) DEFAULT NULL,  `date` date DEFAULT NULL," \
           "  `text` varchar(4000) DEFAULT NULL,  `id` varchar(25) DEFAULT NULL, " \
           " `convID` varchar(25) NOT NULL,  " \
           "`pertinent` tinyint(1) NOT NULL DEFAULT '0') ENGINE=InnoDB DEFAULT CHARSET=latin1;"
    sql3 = "CREATE TABLE IF NOT EXISTS `%s` (`data_id` int(11) NOT NULL," \
           "  `username` varchar(25) DEFAULT NULL,  `date` date DEFAULT NULL," \
           "  `text` varchar(4000) DEFAULT NULL,  `id` varchar(25) DEFAULT NULL " \
           ") ENGINE=InnoDB DEFAULT CHARSET=latin1;"

    connection.autocommit(0)
    for kw in keywords:

        print "=========="
        print kw
        with connection.cursor() as cursor:

            table2 = kw + "_conv"

            sql2 = "CREATE TABLE IF NOT EXISTS `%s` (`data_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT," \
                   "  `username` varchar(25) DEFAULT NULL,  `date` date DEFAULT NULL," \
                   "  `text` varchar(4000) DEFAULT NULL,  `id` varchar(25) DEFAULT NULL, " \
                   " `convID` varchar(25) NOT NULL,  " \
                   "`pertinent` tinyint(1) NOT NULL DEFAULT '0') ENGINE=InnoDB DEFAULT CHARSET=latin1;" % table2
            sql3 = "CREATE TABLE IF NOT EXISTS `%s` (`data_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT," \
                   "  `username` varchar(25) DEFAULT NULL,  `date` date DEFAULT NULL," \
                   "  `text` varchar(4000) DEFAULT NULL,  `id` varchar(25) DEFAULT NULL " \
                   ") ENGINE=InnoDB DEFAULT CHARSET=latin1;" % kw

            cursor.execute(sql3)
            cursor.execute(sql2)

            sql = "INSERT INTO `" + kw + "` (`username`, `date`, `text`, `id`) VALUES (%s,%s,%s,%s)"
            sqlIns2 = "INSERT INTO `" + table2 + "` (`username`, `date`, `text`, `id`, `convID`) VALUES (%s,%s,%s,%s,%s)"

        with open("DATA/New/" + kw + ".csv", "rb") as dataFile:
            spamreader = csv.reader(dataFile, delimiter=';', quotechar='|')
            for row in spamreader:
                user = row[0].replace("\"", "")
                date = row[1].replace("\"", "")
                # print user
                text = row[2].replace("\"", "")
                try:
                    if detect(text) != "en":
                        continue
                except:
                    continue

                id = row[4].replace("\"", "")
                datas = (user, date, text, id)
                with connection.cursor() as cursor:
                    cursor.execute(sql, datas)

        with open("DATA/New/" + kw + "Conversations.csv", "rb") as csvFile:
            spamreader = csv.reader(csvFile, delimiter=';', quotechar='|')
            for row in spamreader:
                # print ', '.join(row)
                # print("Username %s" % row[0])
                # print("Date %s" % row[1])
                # print("Text %s" % row[2])
                # print("HT %s" % row[3])
                # print("ID %s" % row[4])
                user = row[0].replace("\"", "")
                date = row[1].replace("\"", "")
                # print user
                text = row[2].replace("\"", "")
                try:
                    if detect(text) != "en":
                        continue
                except:
                    continue
                id = row[3].replace("\"", "")
                convID = row[4].replace("\"", "")
                datas = (user, date, text, id, convID)
                #
                # with connection.cursor() as cursor:
                with connection.cursor() as cursor:
                    cursor.execute(sqlIns2, datas)
            connection.commit()

        #
        #     try:
        #
        #             # Create a new record
        #         #     sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"
        #         #     cursor.execute(sql, ('webmaster@python.org', 'very-secret'))
        #         #
        #         # # connection is not autocommit by default. So you must commit to save
        #         # # your changes.
        #         # connection.commit()
        #
        #         with connection.cursor() as cursor:
        #             # Read a single record
        #             sql = "SELECT * FROM `data`"
        #             # sql = "SELECT * FROM `data` WHERE `email`=%s"
        #             cursor.execute(sql)
        #             result = cursor.fetchone()
        #             print(result)
        #     finally:
        #         connection.close()


if __name__ == '__main__':
    main()
