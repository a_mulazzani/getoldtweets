import codecs
import datetime
import time
from bs4 import BeautifulSoup

from selenium.webdriver import Firefox
from selenium.webdriver import Chrome
# from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options


class TweetOpener:

    def __init__(self):
        # opts = Options()
        # opts.set_headless()
        # self.tempBrows2 = Firefox(options=opts)

        chrome_options = Options()
        chrome_options.add_argument("--headless")
        self.tempBrows2 = Chrome(chrome_options=chrome_options)

        # print("sono nel self")

    def openFile(self, filename):
        outputFileName = "DATA/New/" + filename + "Conversations.csv"
        self.outputFile = codecs.open(outputFileName, "w+", "utf-8")
        self.outputFile.write('username;date;text;id;convID')

    def close(self):
        self.tempBrows2.close()
        self.outputFile.close()

    def closeFile(self):
        self.outputFile.close()

    def open_tweet(self, id):
        # print(id)

        self.tempBrows2.get('https://twitter.com/statuses/%s' % id)

        # try:
        #     checkMoreReplies = self.tempBrows2.find_elements_by_class_name("ThreadedConversation-showMoreThreadsButton")
        #     for button in checkMoreReplies:
        #         button.click()
        #     checkMoreRepliesInTweets = self.tempBrows2.find_elements_by_class_name("ThreadedConversation-moreRepliesLink")
        #     for button in checkMoreRepliesInTweets:
        #         button.click()
        # except:
        #     print("Exception in the clicking part, continuing")
        try:
            resultsInStatus = self.tempBrows2.find_elements_by_class_name('js-actionable-tweet')
        except Exception:
            return
        # print("Totale Tweet in Status : %d" % len(resultsInStatus))
        if len(resultsInStatus) > 1:
            for result in resultsInStatus:

                try:
                    idPost = result.get_attribute("data-tweet-id")
                    # if id == idPost:
                    #     continue
                    html = result.get_attribute("innerHTML")

                # print(html)

                    soup = BeautifulSoup(html, 'html.parser')

                    # print(soup.prettify())

                    timestamp = soup.find(class_="_timestamp")['data-time']
                    username = soup.find(class_="username").text.replace("@", "")
                    txt = soup.find(class_="tweet-text").text.replace("\n", "")
                    date = datetime.datetime.fromtimestamp(int(timestamp))
                    self.outputFile.write(('\n"%s";"%s";"%s";"%s";%s' % (username, date.strftime("%Y-%m-%d %H:%M"), txt,
                                                                         idPost, id)))
                    # print(timestamp)
                    # print(username)

                except Exception,e:
                    continue
                # date = result.find_element_by_class_name("time")
                # date = date.find_element_by_class_name("tweet-timestamp")
                # date = date.find_element_by_class_name("_timestamp")
                # dateSec = date.get_attribute("data-time")

                # username = result.find_element_by_class_name("username").text.replace("@", "")
                # txt = result.find_element_by_class_name("tweet-text").text.replace("\n", "")

        else:

            self.outputFile.flush()

    def open_tweets(self, convIds):
        i = 0
        for id in convIds:
            try:
                if i % 25 == 0:
                    print("%d / %d -- %s" % (i, len(convIds), datetime.datetime.now().time()))
                i = i + 1

                self.open_tweet(id)

            except Exception, e:
                self.closeFile()
                # print(str(e))
                return
        self.tempBrows2.close()
        self.closeFile()
